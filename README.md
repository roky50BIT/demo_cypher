# Neo4J data loading guide using Cypher
## Step-by-step instructions

These instructions will take you through this process.

![Foo](https://bitbucket.org/roky50BIT/demo_cypher/raw/09e686180985826bb13102b4ae2fa834c5927fa1/process.png)


1. Navigate to [sandbox.neo4j.com]()
1. Create a blank sandbox
1. Once initialised, open browser
1. Validate it's running an empty 

    ```
    MATCH (n) RETURN n
    ```
    
1. Check you can access the files and they are valid CSVs

    ```
    LOAD CSV WITH HEADERS FROM 'https://bitbucket.org/roky50BIT/demo_cypher/raw/d9d5fb85f91ee297e710c4ae93805089cf427df8/SandBox_edges_PC_IS_OFFICER.csv' AS row
    RETURN row
    ```
    
1. In the Neo4J browser drag each of the 7 CQL scripts (all in __CQL__ folder) into the window and drop, click add to favourites in the dialogue
    * You'll need to repeat this for each script as you can't bulk import
    * In the Neo4J browser window you should see the favourites (big star) on the navigation bar on the left.  Click on this to reveal the favourites and you'll see your scripts listed under __Local Scripts__ 

1. Run each of the numbered scripts in order 
    * Do so by clicking the play button next to each one or by clicking the script title so it appears in the code editing window at the top
    * Do not run any script more than once or you might get an error message, extra nodes or edges added

1. Scripts numbered 5 & 6 are for checking content and can be run as many times are you wish
    * Notice there are similar queries under the Data Profiling section of Sample Scripts
    * You should have 2,078 nodes loaded

## Stuff to explore

* You can install some useful python3 libraries and then explore your network in Jupyter with      
```
          python3 -m venv ~/.venv/neo
          source ~/.venv/neo/bin/activate
          pip3 install -r requirements.txt
          jupyter lab
```
* The Neo4J sandbox status page shows you how to connect to your database using Python
    * Try to do this and run some Cypher
    * Now try to find a nice Jupyter notebook plugin to visualise

* Also accessible from the Neo4J sandbox page is Bloom
    * This is a rich graphical user interface that has far more features than the admin browser

* As well as Bloom there are many standalone visualisation products
    * https://neo4j.com/developer/tools-graph-visualization/

***************
## Tips
* Hit return to execute a single line of Cypher
* Shift-return to move to the next line for editing
* Ctrl-enter to execute multi-line statement
* Press Esc to expand query window
* Clear your graph database (delete all nodes and relationships) 

    ```
    MATCH (n) DETACH DELETE n
    ```

* Prefix your query with EXPLAIN to show the execution plan, but NOT execute your query (database won't be changed and no results returned)
* Prefix your query with PROFILE to show the execution activity whilst executing your query (so it will alter the database and/or return results)
* Pin queries to top of browser and re-run using buttons on the top-right of each query window
* Favourite queries and name them by putting // TYPE_NAME__HERE in the line at the top of your query
